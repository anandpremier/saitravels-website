<script>
    function readURL(input) {
        var abc = $('#image').val();
        var fileExtension = ['jpeg', 'jpg', 'png'];
        var abc1 = abc.split('.').pop().toLowerCase();
        var reader = new FileReader();
        if($.inArray(abc1,fileExtension) !== -1)
        {
            $('#image-error').hide();
            if (input.files && input.files[0]) {
                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(auto)
                        .height(auto);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        else
        {
            if (input.files && input.files[0]) {
                reader.onload = function (e) {
                    $('#img')
                        .attr('src', '#')
                        .width(0)
                        .height(0);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    }
</script>
<script>
    $(document).ready(function () {
        $("#reviewForm").validate({
            rules:{
                name:{
                    required:true,
                },
                desc:{
                    required:true,
                },
             
                image:{
                    required:true,
                    extension: "png|jpeg|jpg",
                    accept: "image/jpg,image/jpeg,image/png,image/gif",
                    minImageWidth: 500,
                    maxsize: 200000,
                },
                image1:{
                    extension: "png|jpeg|jpg",
                    accept: "image/jpg,image/jpeg,image/png,image/gif",
                    maxsize: 200000,
                }
            },
            messages:{
                car_name:{
                    required:"Please Enter Name",
                },
                desc:{
                    required:"Please Enter Description ",
                },
              
                image:{
                    required:"Please Select Proper Image",
                    extension: "Invalid File Selection",
                    accept: "Invalid File Selection",
                    maxsize: "Please Select File Less Than 2-Mb",
                },
                image1:{
                    extension: "Invalid File Selection",
                    accept: "Invalid File Selection",
                    maxsize: "Please Select File Less Than 2-Mb",
                }
            }
        });
    });
</script>
<script>
    function deleteCar(input){
        var url = "{{route('admin.review.delete', ":id")}}";
        url = url.replace(':id', input);
        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (data) {
                location.reload();
            }
        });
    }
</script>

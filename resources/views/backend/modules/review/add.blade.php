@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Car Review Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Review</li>
            <li class="active"><a href="{{route('admin.review.add')}}">Add Review</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Review</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="reviewForm" enctype="multipart/form-data" action="{{route('admin.review.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Name:</label>
                                <input type="text" id="name" class="form-control" name="name"
                                       placeholder="Name*:"
                                       value="{{old('name')}}">
                                @if($errors->has('name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Description:</label>
                                <input type="text" id="desc" class="form-control" name="desc"
                                       placeholder="Description*:"
                                       value="{{old('desc')}}">
                                @if($errors->has('desc'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Image:</label>
                                <input type="file" id="image" onchange="readURL(this);" class="form-control" name="image">
                            </div>
                           
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 unique">
                                <img src="" id="img" style="        width: auto;
    height: auto;">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Add Review</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.review.script')
<style>
    .error{
        color:red;
    }
</style>

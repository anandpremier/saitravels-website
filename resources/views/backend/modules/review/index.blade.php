@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Review
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Review</li>
            <li class="active"><a href="{{route('admin.review')}}">Manage Review</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Review </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.review.add')}}">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Review
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($review as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                   
                                    <td>{{$data->desc}}</td>
                                    <td style="text-align: center;"> <img style="width: 70px;border-radius: 100px;" src="<?php echo asset("review/$data->image")?>"></img>

                                   
                                    <td>
                                        <a style="color: green;font-size: x-large;cursor: pointer;" href="{{route('admin.review.edit',$data->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style="color: red;font-size: x-large;cursor: pointer;" onclick="deleteCar({{$data->id}})" data-id="{{$data->id}}">
                                            <i class="fa fa-trash"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.modules.layout.foot')
@include('backend.modules.review.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
        } );
    } );
</script>



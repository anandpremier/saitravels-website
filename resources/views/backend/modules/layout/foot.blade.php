<div class="control-sidebar-bg"></div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ URL::asset('backend/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/morris.js/morris.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ URL::asset('backend/adminlte/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script>
    // $(window).on('load', function() {
    //     $('.content-wrapper').attr('style','min-height:1359px;');
    //     $('.ion').attr('style','margin-top:13px;');
    // });




    $(document).ready(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.editorConfig = function(config) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.height = 300;
            config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('description');

    });
</script>


</body>
</html>

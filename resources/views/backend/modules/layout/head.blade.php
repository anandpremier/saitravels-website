<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ URL::asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('vendor/adminlte/vendor/font-awesome/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('vendor/adminlte/vendor/Ionicons/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('vendor/adminlte/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .error{
            color: red;
        }
       
    </style>
</head>

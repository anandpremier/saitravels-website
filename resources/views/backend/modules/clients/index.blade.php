@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Clients
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>clients</li>
            <li class="active"><a href="{{route('admin.get.clients')}}">List Clients</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"> Enquiry Management </h3>
                    {{--<a class="btn btn-primary pull-right" href="{{route('admin.cars.add')}}">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Car
                    </a>--}}
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>CarName</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>From-Date</th>
                                <th>To-Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data1 as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->first_name}}</td>
                                    <td>{{$data->last_name}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->phone}}</td>
                                    <td>{{$data->from_date}}</td>
                                    <td>{{$data->to_date}}</td>
                                    <td>
                                        <a style="color: green;font-size: x-large;cursor: pointer;" href="{{route('admin.get.show',$data->id)}}">
                                            <i  class="fa fa-eye showData"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
        } );


    } );
</script>

<link rel="stylesheet" href="{{URL::asset('/backend/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('/backend/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('/backend/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('/backend/adminlte/dist/css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('/backend/adminlte/plugins/iCheck/square/blue.css')}}">
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<style>
    .invalid-feedback{
        color: red;
    }
    .valid-feedback{
        color: green;
    }
    .error{
        color: red;
    }
    .content-wrapper {
    float: left !important;
}

</style>

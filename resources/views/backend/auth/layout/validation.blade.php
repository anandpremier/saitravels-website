<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(document).ready(function () {
        $("#adminLoginForm").validate({
            rules:{
                email:{
                    required:true,
                    email:true,
                },
                password:{
                    required:true,
                }
            },
            messages:{
                email:{
                    required:"Please Enter Email To Login",
                    email:"Please Enter Valid Email To Login"
                },
                password:{
                    required:"Please Enter Password To Login"
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#adminResetForm").validate({
            rules:{
                email:{
                    required:true,
                    email:true,
                }
            },
            messages:{
                email:{
                    required:"Please Enter Email To Reset Password",
                    email:"Please Enter Valid Email To Reset Password"
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#adminUpdatePasswordForm").validate({
            rules:{
                email:{
                    required:true,
                    email:true,
                },
                password:{
                    required:true,
                },
                password_confirmation:{
                    required:true,
                    equalTo: "#password"
                }
            },
            messages:{
                email:{
                    required:"Please Enter Email To Reset Password",
                    email:"Please Enter Valid Email To Reset Password"
                },
                password:{
                    required:"Please Enter Password To Reset",
                },
                password_confirmation:{
                    required:"Please Confirm Your Password To Reset",
                    equalTo: "Passwords Not Match"
                }

            }
        });
    });
</script>

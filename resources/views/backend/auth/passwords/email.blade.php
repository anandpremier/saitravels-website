<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @include('backend.auth.layout.header')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b>Password Reset
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter Your Email To Reset Password</p>
        <form id="adminResetForm" method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group has-feedback">
                <input id="email" type="email" class="input--style-2 form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                @if(session('status'))
                    <span class="valid-feedback">
                        <strong>{{ session('status') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
                </div>
            </div>
        </form>
        <br>
        <a href="{{ route('login') }}">I Want To Login</a><br>
        <a href="#" class="text-center">Register a new membership</a>
    </div>
</div>
@include('backend.auth.layout.footer')
@include('backend.auth.layout.validation')
</body>
</html>

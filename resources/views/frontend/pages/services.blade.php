@include('frontend.layouts.header')
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div class="site-wrap" id="home-section">
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
    @include('frontend.layouts.headerNavigation')
    <div class="ftco-blocks-cover-1">
        <div class="ftco-cover-1 overlay innerpage" style="background-image: url({{URL::asset('/frontend/images/hero_2.jpg')}})">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h1>Our Services</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-4 mb-lg-5">
                    <div class="service-1 dark">
                        <span class="service-1-icon">
                            <span class="flaticon-car"></span>
                        </span>
                        <div class="service-1-contents">
                            <h3>Local Ahmedabad Taxi</h3>
                            <p>Spend a day in Ahmedabad, a city whose hospitality has touched the spirits of many. Finding cab services in Ahmedabad is simple. Just book a day's taxi with Jay Ambe travels and shop and tour to your heart's content.
                                We offer a package of 8 hours or 80 kilometers a day. You can extend the mentioned limit at a minimal cost. The additional cost will be calculated on each extra hour spent or each extra kilometers travelled.
                                SConvention, sightseeing or shopping, rest assured that with us you will get the finest taxi service of Ahmedabad.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4 mb-lg-5">
                    <div class="service-1 dark">
                        <span class="service-1-icon">
                            <span class="flaticon-valet-1"></span>
                        </span>
                        <div class="service-1-contents">
                            <h3>Airport Taxi Services</h3>
                            <p>Have you arrived at Ahmedabad airport at 1 AM and found it difficult to locate a taxi near you? Don't worry, and book a cab with Jay Ambe Travels. Providing 24*7 taxi service for airports is our forte. You can hire us for one side transfer or for a to and fro journey. We are one of the top cab services in Ahmedabad and for us your convenience and safety is paramount.
                                For your back to back meetings rely on us. Our well-mannered drivers will make your work day a smooth sailing affair. You can also plan a day trip with us and with visit the city for leisure.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4 mb-lg-5">
                    <div class="service-1 dark">
                        <span class="service-1-icon">
                            <span class="flaticon-key"></span>
                        </span>
                        <div class="service-1-contents">
                            <h3>Corporate Taxi Services</h3>
                            <p>Do you frequently have outstation colleagues and associates visiting your Ahmedabad office? Does your staff regularly travel to Baroda for meetings?
                                For all your travel needs tie-up with Jay Ambe Travels. Our car rental service for corporates gives you the ease to book a taxi at the last minute at a predefined price. This saves you the hassle of finding a new cab vendor every time.
                                In addition to this, punctuality is a vital part of our system, and that is why we are serving several big business houses in Ahmedabad. Be it a board meeting, a conference or a corporate dinner, with us you will surely reach your destination on time.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container site-section mb-5">
        <div class="row justify-content-center text-center">
            <div class="col-7 text-center mb-5">
                <h2>How it works</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo assumenda, dolorum necessitatibus eius earum voluptates sed!</p>
            </div>
        </div>
        <div class="how-it-works d-flex">
            <div class="step">
                <span class="number"><span>01</span></span>
                <span class="caption">Time &amp; Place</span>
            </div>
            <div class="step">
                <span class="number"><span>02</span></span>
                <span class="caption">Car</span>
            </div>
            <div class="step">
                <span class="number"><span>03</span></span>
                <span class="caption">Details</span>
            </div>
            <div class="step">
                <span class="number"><span>04</span></span>
                <span class="caption">Checkout</span>
            </div>
            <div class="step">
                <span class="number"><span>05</span></span>
                <span class="caption">Done</span>
            </div>

        </div>
    </div>
    @include('frontend.layouts.footerNavigation')
</div>
@include('frontend.layouts.footer')
</body>
</html>


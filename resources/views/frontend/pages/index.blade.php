@include('frontend.layouts.header')
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div class="site-wrap" id="home-section">
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
    @include('frontend.layouts.headerNavigation')
    <div class="ftco-blocks-cover-1">
        <div class="mobileImg" style="background-image: url('{{URL::asset('/frontend/images/hero_1.jpg')}}')"></div>
        <div class="your-class">
            <div>
                <div class="ftco-cover-1 overlay" style="background-image: url({{URL::asset('/frontend/images/hero_1.jpg')}}">

                </div>
            </div>
            <div>
                <div class="ftco-cover-1 overlay" style="background-image: url({{URL::asset('/frontend/images/hero-2.jpg')}}">

                </div>
            </div>
            <div>
                <div class="ftco-cover-1 overlay" style="background-image: url({{URL::asset('/frontend/images/hero-3.jpg')}}">

                </div>
            </div>
        </div>

    </div>
    <div class="site-section pt-0 pb-0 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <form class="trip-form">
                        <div class="row align-items-center mb-4">
                            <div class="col-md-6">
                                <h3 class="m-0">Begin your trip here</h3>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <span class="text-primary">{{$carsCount}}</span> <span>cars available</span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label for="cf-1">Where you from</label>
                                <input type="text" id="cf-1" placeholder="Your pickup address" class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cf-2">Where you go</label>
                                <input type="text" id="cf-2" placeholder="Your drop-off address" class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cf-3">Journey date</label>
                                <input type="text" id="cf-3" placeholder="Your pickup address" class="form-control datepicker px-3">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cf-4">Return date</label>
                                <input type="text" id="cf-4" placeholder="Your pickup address" class="form-control datepicker px-3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="submit" value="Submit" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h3>Our Offer</h3>
                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure nesciunt nemo vel earum maxime neque!</p>
                    <p>
                        <a href="#" class="btn btn-primary custom-prev">Previous</a>
                        <span class="mx-2">/</span>
                        <a href="#" class="btn btn-primary custom-next">Next</a>
                    </p>
                </div>
                <div class="col-lg-9">
                    <div class="nonloop-block-13 owl-carousel">
                        @foreach($cars as $data)
                            <div class="item-1">
                                <a href="#"><img src="{{URL::asset('/cars')."/".$data->image}}" alt="Image" class="img-fluid" style="height: 253px;width: 407px;"></a>
                                <div class="item-1-contents">
                                    <div class="text-center">
                                        <h3><a href="#"></a>{{$data->name}}</h3>
                                        <div class="rating">
                                            <span class="icon-star text-warning"></span>
                                            <span class="icon-star text-warning"></span>
                                            <span class="icon-star text-warning"></span>
                                            <span class="icon-star text-warning"></span>
                                            <span class="icon-star text-warning"></span>
                                        </div>
                                        <div class="rent-price"><span>₹ 250/</span>day</div>
                                    </div>
                                    <ul class="specs">
                                        <li>
                                            <span>Doors</span>
                                            <span class="spec">4</span>
                                        </li>
                                        <li>
                                            <span>Seats</span>
                                            <span class="spec">{{$data->seat}}</span>
                                        </li>
                                        <li>
                                            <span>AC-Availability</span>
                                            <span class="spec">@if($data->ac)Available @else Not-Available @endif</span>
                                        </li>
                                        {{--<li>
                                            <span>Minium age</span>
                                            <span class="spec">18 years</span>
                                        </li>--}}
                                    </ul>
                                    <div class="d-flex action">
                                        <a href="{{route('contact',$data->id)}}" class="btn btn-primary">Rent Now</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section section-3" style="background-image: url({{URL::asset('/frontend/images/hero_2.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb-5">
                    <h2 class="text-white">Our services</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="service-1">
              <span class="service-1-icon">
                <span class="flaticon-car-1"></span>
              </span>
                        <div class="service-1-contents">
                            <h3>Local Ahmedabad Taxi</h3>
                            <p>Spend a day in Ahmedabad, a city whose hospitality has touched the spirits of many. Finding cab services in Ahmedabad is simple. Just book a day's taxi with Jay Ambe travels and shop and tour to your heart's content.
                                We offer a package of 8 hours or 80 kilometers a day. You can extend the mentioned limit at a minimal cost. The additional cost will be calculated on each extra hour spent or each extra kilometers travelled.
                                SConvention, sightseeing or shopping, rest assured that with us you will get the finest taxi service of Ahmedabad.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-1">
              <span class="service-1-icon">
                <span class="flaticon-traffic"></span>
              </span>
                        <div class="service-1-contents">
                            <h3>Airport Taxi Services</h3>
                            <p>Have you arrived at Ahmedabad airport at 1 AM and found it difficult to locate a taxi near you? Don't worry, and book a cab with Jay Ambe Travels. Providing 24*7 taxi service for airports is our forte. You can hire us for one side transfer or for a to and fro journey. We are one of the top cab services in Ahmedabad and for us your convenience and safety is paramount.
                                For your back to back meetings rely on us. Our well-mannered drivers will make your work day a smooth sailing affair. You can also plan a day trip with us and with visit the city for leisure.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-1">
              <span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span>
                        <div class="service-1-contents">
                            <h3>Corporate Taxi Services</h3>
                            <p>Do you frequently have outstation colleagues and associates visiting your Ahmedabad office? Does your staff regularly travel to Baroda for meetings?
                                For all your travel needs tie-up with Jay Ambe Travels. Our car rental service for corporates gives you the ease to book a taxi at the last minute at a predefined price. This saves you the hassle of finding a new cab vendor every time.
                                In addition to this, punctuality is a vital part of our system, and that is why we are serving several big business houses in Ahmedabad. Be it a board meeting, a conference or a corporate dinner, with us you will surely reach your destination on time.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container site-section mb-5">
        <div class="row justify-content-center text-center">
            <div class="col-7 text-center mb-5">
                <h2>How it works</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo assumenda, dolorum necessitatibus eius earum voluptates sed!</p>
            </div>
        </div>
        <div class="how-it-works d-flex">
            <div class="step">
                <span class="number"><span>01</span></span>
                <span class="caption">Time &amp; Place</span>
            </div>
            <div class="step">
                <span class="number"><span>02</span></span>
                <span class="caption">Car</span>
            </div>
            <div class="step">
                <span class="number"><span>03</span></span>
                <span class="caption">Details</span>
            </div>
            <div class="step">
                <span class="number"><span>04</span></span>
                <span class="caption">Checkout</span>
            </div>
            <div class="step">
                <span class="number"><span>05</span></span>
                <span class="caption">Done</span>
            </div>

        </div>
    </div>
    <div class="site-section bg-light">
        <div class="container">
            <div class="row justify-content-center text-center mb-5">
                <div class="col-7 text-center mb-5">
                    <h2>Customer Testimony</h2>
                    <p>The service was excellent - thank you. My driver was waiting at Arrivals for me with a clear sign. He introduced himself, was very polite and friendly and drove me to my hotel with no delay. I will be pleased to recommend this service to my family and friends.
                        <br>Date of Posting: 29 October 2019
                        Posted By: Ashna Brahmbhatt,
                        Ahmedabad!</p>
                </div>
            </div>
            <div class="row">

            @foreach($review as $value)
                <div class="col-lg-4 mb-4 mb-lg-0">
                    <div class="testimonial-2">
                        <blockquote class="mb-4">
                            <p>{{$value->desc}}</p>
                        </blockquote>
                        <div class="d-flex v-card align-items-center">
                       <img src="{{URL::asset('/review')."/".$value->image}}" alt="Image" class="img-fluid img-fluid mr-3">
                            <span>{{$value->name}}</span>
                        </div>
                    </div>
                </div>
             @endforeach
               
            </div>
        </div>
    </div>
    {{--<div class="site-section bg-white">
        <div class="container">
            <div class="row justify-content-center text-center mb-5">
                <div class="col-7 text-center mb-5">
                    <h2>Our Blog</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo assumenda, dolorum necessitatibus eius earum voluptates sed!</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="post-entry-1 h-100">
                        <a href="{{route('single')}}">
                            <img src="{{URL::asset('/frontend/images/post_1.jpg')}}" alt="Image"
                                 class="img-fluid">
                        </a>
                        <div class="post-entry-1-contents">
                            <h2><a href="{{route('single')}}">The best car rent in the entire planet</a></h2>
                            <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="post-entry-1 h-100">
                        <a href="{{route('single')}}">
                            <img src="{{URL::asset('/frontend/images/img_2.jpg')}}" alt="Image"
                                 class="img-fluid">
                        </a>
                        <div class="post-entry-1-contents">

                            <h2><a href="{{route('single')}}">The best car rent in the entire planet</a></h2>
                            <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="post-entry-1 h-100">
                        <a href="{{route('single')}}">
                            <img src="{{URL::asset('/frontend/images/img_3.jpg')}}" alt="Image"
                                 class="img-fluid">
                        </a>
                        <div class="post-entry-1-contents">

                            <h2><a href="{{route('single')}}">The best car rent in the entire planet</a></h2>
                            <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    @include('frontend.layouts.footerNavigation')
</div>
@include('frontend.layouts.footer')
</body>
</html>


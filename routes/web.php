<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontend\DesignController@index')->name('index');
Route::get('/services','frontend\DesignController@services')->name('services');
Route::get('/car', 'frontend\DesignController@cars')->name('cars');
Route::get('/about', 'frontend\DesignController@about')->name('about');
/*Route::get('/blog', function () {return view('frontend.pages.blog');})->name('blog');*/
Route::get('/single', function () {return view('frontend.pages.single');})->name('single');
Route::get('/contact/{id?}', 'frontend\DesignController@contact')->name('contact');
Route::post('/contact/store', 'frontend\DesignController@contactStore')->name('client.store');
Route::post('/get/cars', 'frontend\DesignController@getCars')->name('get.cars');
Route::get('admin', 'Auth\LoginController@showLoginForm');
Route::post('admin', 'Auth\LoginController@login');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'AuthenticateUser','namespace'=>'backend','prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/dashboard','HomeController@index')->name('dashboard');
    Route::get('/cars/manage','CarsController@index')->name('cars.manage');
    Route::get('/cars/add','CarsController@create')->name('cars.add');
    Route::post('/cars/store','CarsController@store')->name('cars.store');
    Route::post('/cars/delete/{id}','CarsController@destroy')->name('cars.delete');
    Route::get('/cars/edit/{id}','CarsController@edit')->name('cars.edit');
    Route::get('/cars/get/','ClientController@index')->name('get.clients');
    Route::get('/cars/show/{id}','ClientController@show')->name('get.show');

    Route::get('/cms/about','CmsController@index')->name('cms.about');
    Route::get('/cms/about/add','CmsController@create')->name('cms.add');
    Route::post('/cms/about/store','CmsController@store')->name('cms.store');
    Route::get('/cms/about/edit/{id}','CmsController@edit')->name('cms.edit');
    Route::post('/cms/about/delete/{id}','CmsController@destroy')->name('cms.delete');

    Route::get('/cms/service','CmsController@serviceindex')->name('cms.service');
    Route::get('/cms/service/add','CmsController@servicecreate')->name('cms.service.add');
    Route::post('/cms/service/store','CmsController@servicestore')->name('cms.service.store');
    Route::get('/cms/service/edit/{id}','CmsController@serviceedit')->name('cms.service.edit');
    Route::post('/cms/service/delete/{id}','CmsController@servicedestroy')->name('cms.service.delete');

    Route::get('/cms/contact','CmsController@contactindex')->name('cms.contact');
    Route::get('/cms/contact/add','CmsController@contactcreate')->name('cms.contact.add');
    Route::post('/cms/contact/store','CmsController@contactstore')->name('cms.contact.store');
    Route::get('/cms/contact/edit/{id}','CmsController@contactedit')->name('cms.contact.edit');
    Route::post('/cms/contact/delete/{id}','CmsController@contactdestroy')->name('cms.contact.delete');

    Route::get('/seo','SeoController@index')->name('seo');
    Route::get('/seo/add','SeoController@create')->name('seo.add');
    Route::post('/seo/store','SeoController@store')->name('seo.store');
    Route::get('/seo/edit/{id}','SeoController@edit')->name('seo.edit');
    Route::post('/seo/delete/{id}','SeoController@destroy')->name('seo.delete');

    Route::get('/review/manage','ReviewController@index')->name('review');
    Route::get('/review/add','ReviewController@create')->name('review.add');
    Route::post('/review/store','ReviewController@store')->name('review.store');
    Route::post('/review/delete/{id}','ReviewController@destroy')->name('review.delete');
    Route::get('/review/edit/{id}','ReviewController@edit')->name('review.edit');
   
});

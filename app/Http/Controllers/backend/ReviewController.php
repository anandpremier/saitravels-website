<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\backend\Review;
use Illuminate\Support\Facades\File;


class ReviewController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = Review::get();
        return view('backend.modules.review.index')->with(compact('review'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.review.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
       
        if(isset($request->editid)){
            $request->validate([
                'name' => 'required',
                'desc'=> 'required',
            ]);
            $obj = Review::findorfail($request->editid);
        }else{
            $request->validate([
                'name' => 'required',
                'desc'=> 'required',
            ]);
            $obj = new Review();
        }
        if($request->file('image') != null){
            $ext = $request->file('image')->getClientOriginalExtension();
            $file = $request->file('image');
            $path = md5($file->getClientOriginalName() . time()) . "." . $ext;
            $file->move(public_path('review'), $path);
        }
        if(isset($request->editid)){
            if($request->file('image1') != null){
                $image_path = public_path('review')."/".$obj->image;  // Value is not URL but directory file path
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $ext = $request->file('image1')->getClientOriginalExtension();
                $file = $request->file('image1');
                $path = md5($file->getClientOriginalName() . time()) . "." . $ext;
                $file->move(public_path('review'), $path);
            }
        }

        $obj->name = $request['name'];
        $obj->desc = $request['desc'];
      
        if(isset($path))
            $obj->image = $path;

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');


        return redirect()->route('admin.review');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::where('id',$id)->get();
        return view('backend.modules.review.edit')->with(compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::where('id',$id)->delete();
        return response()->json(1);
    }
}

<?php

namespace App\Http\Controllers\backend;

use App\Models\backend\Cars;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        $carsCount = Cars::get()->count();
        return view('backend.modules.dashboard')->with(compact('carsCount'));
    }
}

<?php

namespace App\Http\Controllers\backend;

use App\Models\backend\Cars;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Cars::get();
        return view('backend.modules.cars.index')->with(compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.cars.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        if(isset($request->editid)){
            $request->validate([
                'car_name' => 'required|unique:cars_data,name,'.$request->editid,
                'seats'=> 'integer|required',
                'ac'=> 'integer|required',
            ]);
            $obj = Cars::findorfail($request->editid);
        }else{
            $request->validate([
                'car_name' => 'required|unique:cars_data,name,',
                'seats'=> 'integer|required',
                'ac'=> 'integer|required',
                'status'=> 'required',
            ]);
            $obj = new Cars();
        }
        if($request->file('image') != null){
            $ext = $request->file('image')->getClientOriginalExtension();
            $file = $request->file('image');
            $path = md5($file->getClientOriginalName() . time()) . "." . $ext;
            $file->move(public_path('cars'), $path);
        }
        if(isset($request->editid)){
            if($request->file('image1') != null){
                $image_path = public_path('cars')."/".$obj->image;  // Value is not URL but directory file path
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $ext = $request->file('image1')->getClientOriginalExtension();
                $file = $request->file('image1');
                $path = md5($file->getClientOriginalName() . time()) . "." . $ext;
                $file->move(public_path('cars'), $path);
            }
        }

        $obj->name = $request['car_name'];
        $obj->seat = $request['seats'];
        $obj->ac = $request['ac'];
        $obj->status = $request['status'];
        if(isset($path))
            $obj->image = $path;

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');


        return redirect()->route('admin.cars.manage');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cars = Cars::where('id',$id)->get();
        return view('backend.modules.cars.edit')->with(compact('cars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cars::where('id',$id)->delete();
        return response()->json(1);
    }
}

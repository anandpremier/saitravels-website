<?php

namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = "register_client";
    protected $fillable = ['car_id','first_name','last_name','email','phone','description','from_date','to_date'];
    protected $primaryKey="id";
}

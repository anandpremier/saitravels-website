<?php

namespace App\Models\backend;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "review";
    protected $fillable = ['name','image','desc','created_at','updated_at'];
    protected $primaryKey="id";
}

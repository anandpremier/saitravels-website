<?php

namespace App\Models\backend;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = "cms_page";
    protected $fillable = ['page_name','description','status'];
    protected $primaryKey="id";
}
